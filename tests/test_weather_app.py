from unittest.mock import MagicMock
import pytest
from requests.exceptions import ConnectionError, HTTPError

from src.HoeWarmIsHetInDelft import WeatherStatus, NoValidData, ConnectionFailed


@pytest.fixture()
def response():
    return MagicMock()


@pytest.fixture()
def requests_get(response):
    return MagicMock(return_value=response)


@pytest.fixture()
def weather_status(requests_get):
    return WeatherStatus('weather_data_url', requests_get)


def test_failed_server_connection(requests_get, weather_status):
    requests_get.side_effect = ConnectionError

    with pytest.raises(ConnectionFailed):
        weather_status.collect_weather_data()


def test_http_error_status_code(response, weather_status):
    response.raise_for_status.side_effect = HTTPError

    with pytest.raises(ConnectionFailed):
        weather_status.collect_weather_data()


@pytest.mark.parametrize(
    "content",
    [b'', b'123456 a1 a2 a3 15.5 !! ',  b'12345 a1 a2 a3 !!', b'12345 a1 a2 a3 15.5 !!z']
)
def test_process_weather_data_for_invalid_response_content(response, weather_status, content):
    response.status_code = 200
    response.content = content

    with pytest.raises(NoValidData):
        weather_status.collect_weather_data()


def test_get_correct_temperature_from_valid_data(response, weather_status):
    response.status_code = 200
    response.content = b'12345 a1 a2 a3 15.5 !! '
    weather_status.collect_weather_data()

    assert weather_status.get_temperature() == 15.5
