import requests
import logging
import sys
from collections.abc import Callable

URL_TO_DATA_FILE = r'https://www.weerindelft.nl/clientraw.txt'

logger = logging.getLogger(__name__)
logger.setLevel(logging.WARNING)
stderr_handler = logging.StreamHandler(sys.stderr)
stderr_handler.setFormatter(logging.Formatter(fmt="%(asctime)s %(name)s %(levelname)s %(message)s"))
logger.addHandler(stderr_handler)


class WeatherException(Exception):

    """Weater general Exception"""


class ConnectionFailed(WeatherException):

    """Connection to server failed or HTTP SUCCESS status was not returned"""

    def __str__(self) -> str:
        return "Server connection failed."


class NoValidData(WeatherException):

    """Weather data file is not valid"""

    def __str__(self) -> str:
        return "Received weather data file is not valid."


class WeatherStatus:
    """Class for extracting weather data from request websites using file format like on www.weerindelft.nl"""

    RAW_DATA_FIRST_ELEMENT = '12345'
    RAW_DATA_LAST_2_CHARS = '!!'
    RAW_DATA_TEMPERATURE_POSITION = 4

    def __init__(self, data_file_url: str, request_method: Callable[[str], requests.Response]) -> None:
        self._data_file_url = data_file_url
        self._request_data = request_method
        self._weather_data_table: list[str] = []

    def collect_weather_data(self) -> None:
        """Collect weather data from clientraw.txt file on HTTP server."""
        try:
            self._get_weather_data()
        except (requests.exceptions.ConnectionError, requests.exceptions.Timeout, requests.exceptions.HTTPError):
            logger.error(ConnectionFailed())
            raise ConnectionFailed
        if not self._weather_data_is_valid():
            logger.error(f'{NoValidData()}: {self._weather_data_table}')
            raise NoValidData from None

    def get_temperature(self) -> float:
        """Extract temperature from weather data"""
        raw_temperature = self._weather_data_table[self.RAW_DATA_TEMPERATURE_POSITION]
        return float(raw_temperature)

    def _get_weather_data(self) -> None:
        response = self._request_data(self._data_file_url)
        response.raise_for_status()
        self._weather_data_table = response.content.decode().split()

    def _weather_data_is_valid(self) -> bool:
        """Check if weather data from clientraw.txt file on HTTP server are available and in correct format."""
        valid_first_element = valid_last_element = False
        if len(self._weather_data_table) >= 6 and len(self._weather_data_table[-1]) >= len(self.RAW_DATA_LAST_2_CHARS):
            valid_first_element = self._weather_data_table[0] == self.RAW_DATA_FIRST_ELEMENT
            valid_last_element = self._weather_data_table[-1][-2:] == self.RAW_DATA_LAST_2_CHARS
        return valid_first_element and valid_last_element


def main() -> None:
    weather_in_delft = WeatherStatus(URL_TO_DATA_FILE, requests.get)
    weather_in_delft.collect_weather_data()
    temperature_in_delft = round(weather_in_delft.get_temperature())
    print(f'{temperature_in_delft} degrees Celsius')


if __name__ == '__main__':
    main()
