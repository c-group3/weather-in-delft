# Weather-in-Delft

Docker container with Python script that retrieves from http://www.weerindelft.nl/ current temperature in Delft and prints it to standard output, rounded to degrees Celsius. Or reason why temperature could not be obtained.

## TODO
- application versioning
- docker image tag based on application version
- CI pipeline artifacts
