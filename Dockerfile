FROM  python:3.11.4-alpine3.18

LABEL Name="Python Weather in Delft App"

ARG srcDir=src
WORKDIR /app
COPY $srcDir/requirements.txt .
RUN pip install --no-cache-dir --upgrade pip
RUN pip install --no-cache-dir -r requirements.txt

COPY $srcDir/HoeWarmIsHetInDelft.py .

CMD ["python", "HoeWarmIsHetInDelft.py"]
